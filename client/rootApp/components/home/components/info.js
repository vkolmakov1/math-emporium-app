import React from 'react';

export default () => (
    <div className="info-home">
      <div>
        <div className="fa-wrap">
          <i className="fa fa-5x fa-minus-circle" aria-hidden="true"></i>
        </div>
        <h1>Here be dragons</h1>
        <p>This application is not in a production-ready state. For a fully-functional app visit: <a href="http://mathcenter.herokuapp.com">http://mathcenter.herokuapp.com</a></p>
      </div>
    </div>
);
