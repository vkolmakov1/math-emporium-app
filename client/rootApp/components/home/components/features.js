import React from 'react';

export default () => (
    <div className="section-home">
      <div className="section-home-item">
        <div className="fa-wrap">
          <i className="fa fa-5x fa-envelope-o" aria-hidden="true"></i>
        </div>
        <p>Sign up with your school email address</p>
      </div>
      <div className="section-home-item">
        <div className="fa-wrap">
          <i className="fa fa-5x fa-calendar-check-o" aria-hidden="true"></i>
        </div>
        <p>Schedule an appointment</p>
      </div>
      <div className="section-home-item">
        <div className="fa-wrap">
          <i className="fa fa-5x fa-thumbs-o-up" aria-hidden="true"></i>
        </div>
        <p>And you're all set! Come in for your appointment at the selected time</p>
      </div>
    </div>
);
