import React from 'react';
import Hero from './components/hero';
import Features from './components/features';
import Info from './components/info';

export default () => (
    <div>
      <Hero />
      <Features />
      <Info />
    </div>
);
