import * as constants from '../constants';

export const BASE_PATH = 'schedule';
export const RANDOM_TUTOR = { id: 'RANDOM', name: `I'm feeling lucky!` };

export const TIME_OPTIONS = constants.TIME_OPTIONS;
export const TIMESTAMP_FORMAT = constants.TIMESTAMP_FORMAT;
export const WEEKDAY_OPTIONS = constants.WEEKDAY_OPTIONS;
export const TIMESTAMP_DISPLAY_FORMAT = constants.TIMESTAMP_DISPLAY_FORMAT;
export const AUTH_GROUPS = constants.AUTH_GROUPS;
